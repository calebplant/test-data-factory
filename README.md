# Data Factory

## Overview

A class for simplifying the process of dynamically generating test data. Confirmed to work with Account/Contact/Product/Lead/Opportunity, but should (currently) theoretically work with any object that does have a required field that is a lookup to another object.

## File Overview

### Apex

* **DataFactory** - Core class that the user calls. Possesses methods for generating sobjects with or without specifying field values and in single or multiple quantities. *See example section below*.
* **DataFactoryHelper** - Helper class for DataFactory that populates field values while generating sobjects.
* **DataFactoryDefaultValues** - Helper class for DataFactoryHelper that handles automatically generating values for required/marked fields of the sobject.
* **ClassFieldInfo** - Wrapper class that holds data about the nature of the generted object's fields (required, relationship, etc.)

## Example Usage

### Automatic population of required fields

#### In

```apex
Account acc = (Account)DataFactory.generateObject('Account');
Contact con = (Contact)DataFactory.generateObject('Contact');
Lead theLead = (Lead)DataFactory.generateObject('Lead');
Opportunity opp = (Opportunity)DataFactory.generateObject('Opportunity');
Product2 prod = (Product2)DataFactory.generateObject('Product2');

List<Contact> cons = (List<Contact>)DataFactory.generateObjectList('Contact', 2);
```

#### Out
```
Account:{Name=defaultText:0}
Contact:{LastName=defaultText:0}
Lead:{LastName=defaultText:0, Company=defaultText:0}
Opportunity:{Name=defaultText:0, StageName=Prospecting, CloseDate=2020-10-12 00:00:00}
Product2:{Name=defaultText:0}

(Contact:{LastName=defaultText:0}, Contact:{LastName=defaultText:1})
```

### Manually setting fields

#### In

```apex
Contact con = (Contact)DataFactory.generateObject('Contact', new Map<String, Object>{
            'FirstName' => 'Frank'
        });

List<Contact> cons = (List<Contact>)DataFactory.generateObjectList('Contact', 2, new Map<String, Object>{
    'FirstName' => 'Frank'
});
```

#### Out

```
Contact:{LastName=defaultText:0, FirstName=Frank}

(Contact:{LastName=defaultText:0, FirstName=Frank}, Contact:{LastName=defaultText:1, FirstName=Frank})
```

### Mark optional field to generate a value

#### In

```apex
Contact con = (Contact)DataFactory.generateObject('Contact', new Map<String, Object>{
            'FirstName' => 'Frank',
            'Description' => DataFactory.POPULATE
        });
```

#### Out

```
Contact:{LastName=defaultText:0, FirstName=Frank, Description=defaultTextArea:927}
```

### Create sobject with associated sobject

#### In

```apex
List<Contact> cons = (List<Contact>)DataFactory.generateObjectList('Contact', 2, new Map<String, Object> {
            'FirstName' => 'Frank',
            'Account' => new Map<String, Object> {
                'Name' => 'Fort'
            }
        });
```

#### Out

```
(Contact:{Id=0033B00000V8hUfQAJ, AccountId=0013B00000e5SGjQAM}, Contact:{Id=0033B00000V8hUgQAJ, AccountId=0013B00000e5SGjQAM})

Account:{Id=0013B00000e5SGjQAM, Name=Fort}
```