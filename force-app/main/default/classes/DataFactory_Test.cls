@isTest
public with sharing class DataFactory_Test {
    
    @isTest
    static void doesGenerateDefaultRequiredValuesForSingleSObject()
    {
        List<sObject> objs = new List<Sobject>();

        Test.startTest();
        Account acc = (Account)DataFactory.generateObject('Account');
        Contact con = (Contact)DataFactory.generateObject('Contact');
        Lead theLead = (Lead)DataFactory.generateObject('Lead');
        Opportunity opp = (Opportunity)DataFactory.generateObject('Opportunity');
        Product2 prod = (Product2)DataFactory.generateObject('Product2');
        Test.stopTest();

        // Account
        System.assertNotEquals(acc.Name, null);
        // Contact
        System.assertNotEquals(con.LastName, null);
        // Lead
        System.assertNotEquals(theLead.LastName, null);
        System.assertNotEquals(theLead.Company, null);
        // Opportunity
        System.assertNotEquals(opp.Name, null);
        System.assertNotEquals(opp.StageName, null);
        System.assertNotEquals(opp.CloseDate, null);
        // Product2
        System.assertNotEquals(prod.Name, null);
    }

    @isTest
    static void doesGenerateOptionalManualFieldsForSingleObject()
    {
        Test.startTest();
        Contact con = (Contact)DataFactory.generateObject('Contact', new Map<String, Object>{
            'FirstName' => 'Frank'
        });
        Test.stopTest();

        System.assertEquals('Frank', con.FirstName);
    }

    @isTest
    static void doesGenerateListWithRequiredValues()
    {
        Test.startTest();
        List<Contact> cons = (List<Contact>)DataFactory.generateObjectList('Contact', 10);
        Test.stopTest();

        System.assertEquals(10, cons.size());
        System.assertNotEquals(cons[0].LastName, null);
    }

    @isTest
    static void doesGenerateOptionalManualValuesForList()
    {
        Test.startTest();
        List<Contact> cons = (List<Contact>)DataFactory.generateObjectList('Contact', 10, new Map<String, Object>{
            'FirstName' => 'Frank'
        });
        Test.stopTest();

        System.assertEquals(10, cons.size());
        System.assertEquals('Frank', cons[0].FirstName);
    }

    @isTest
    static void doesGenerateManualAssociatedObject()
    {
        Integer startAccs = [SELECT COUNT() FROM Account];

        Test.startTest();
        Contact con = (Contact)DataFactory.generateObject('Contact', new Map<String, Object> {
            'FirstName' => 'Frank',
            'Account' => new Map<String, Object> {
                'Name' => 'Fort'
            }
        });
        Test.stopTest();

        System.assertEquals('Frank', con.FirstName);
        List<Account> passedAcc = [SELECT Id, Name FROM Account WHERE Name = 'Fort'];
        System.assertNotEquals(0, passedAcc.size());
        System.assertEquals('Fort', passedAcc[0].Name);
    }

    @isTest
    static void doesGenerateManualAssociatedObjectForList()
    {
        Test.startTest();
        List<Contact> cons = (List<Contact>)DataFactory.generateObjectList('Contact', 2, new Map<String, Object> {
            'FirstName' => 'Frank',
            'Account' => new Map<String, Object> {
                'Name' => 'Fort'
            }
        });
        insert cons;
        Test.stopTest();

        System.assertEquals('Frank', cons[0].FirstName);
        List<Account> passedAcc = [SELECT Id, Name FROM Account WHERE Name = 'Fort'];
        System.assertNotEquals(0, passedAcc.size());
        System.assertEquals('Fort', passedAcc[0].Name);
        cons = [SELECT Id, Account.Name FROM Contact WHERE FirstName = 'Frank'];
        System.assertEquals('Fort', cons[0].Account.Name);

        System.debug(cons);
        System.debug(passedAcc);
    }

    @isTest
    static void doesPopulateValueCauseOptionalFieldToPopulateInSingleObject()
    {
        Test.startTest();
        Contact con = (Contact)DataFactory.generateObject('Contact', new Map<String, Object>{
            'FirstName' => 'Frank',
            'Description' => DataFactory.POPULATE
        });
        Test.stopTest();
        
        System.assertNotEquals(null, con.Description);
        System.assert(con.Description.startsWith('defaultTextArea'));
    }

    @isTest
    static void doesValidateManualFieldNamesThrowError()
    {
        Boolean exceptionThrown = false;
        Test.startTest();
        try {
            Contact con = (Contact)DataFactory.generateObject('Contact', new Map<String, Object>{
                'FieldDoesntExist' => 'Frank'
            });
        } catch(DataFactoryException e) {
            exceptionThrown = true;
        }
        Test.stopTest();

        System.assert(exceptionThrown);
    }
}
