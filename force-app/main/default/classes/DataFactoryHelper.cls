public with sharing class DataFactoryHelper {

    public static sObject nestedObject;
    private static String nestedObjName;

    /*
        Populates passed required fields for an sobject and returns it

        Parameters:
        @    obj - SObject we are updating
        @    index - Integer used for creating unique values in DataFactoryDefaultValues
        @    requiredFields - List of required fields we need to populate
    */
    public static SObject populateRequiredFields(sObject obj, Integer index, List<Schema.DescribeFieldResult> requiredFields)
    {
        SObject result = obj;

        for(Schema.DescribeFieldResult eachField : requiredFields) {
             // Does not have default create value?
            if(!eachField.isDefaultedOnCreate()) {
                result.put(eachField.getName(), DataFactoryDefaultValues.getValue(eachField, index)); // Generate a value
            }
        }
        return result;
    }

    /*
        Populates passed fields for an sobject and returns it

        Parameters:
        @    obj - SObject we are updating
        @    manualValues - Map of String=>Object for manually setting sobject values.
             Example for Contact with parent Account to generate:
                new Map<String, Object> {
                    'FirstName' => 'Frank',
                    'LastName' => 'Smith',
                    'Account' => new Map<String, Object> {
                        'Name' => 'Fort'
                    }
                }
        @    fieldInfo - ClassFieldInfo that contains information about the sobject's fields (required, relationship, etc)
    */
    public static SObject populateManualFields(sObject obj, Map<String, Object> manualValues, ClassFieldInfo fieldInfo)
    {
        SObject result = obj;

        for(String eachKey : manualValues.keySet()) {
            if(!fieldInfo.relationshipFieldByName.keySet().contains(eachKey)) { // Not a relationship field
                // Check if field should generate a default value (i.e. was passed value of DataFactory.POPULATE)
                if(fieldInfo.optionalFieldByName.keySet().contains(eachKey)) {
                    result.put(eachKey, DataFactoryDefaultValues.getValue(fieldInfo.optionalFieldByName.get(eachKey), DateTime.now().millisecond())); // Set to generated value
                } else {
                    result.put(eachKey, manualValues.get(eachKey)); // Set to passed value
                }
            } else { // is relationship field
                // Generate nestedobject and record it. Will insert and associate later
                nestedObject = DataFactory.generateObject(eachKey, (Map<String, Object>)manualValues.get(eachKey));
                nestedObjName = eachKey;
            }
        }
        return result;
    }

    /*
        Check if passed values are valid field names. Throws a DataFactoryException error if not.

        Parameters:
        @    manualValueFieldNames - Passed manual field names. Ex: 'FirstName'
        @    objType - sObjectType we are checking
        @    relationshipFields - List of relationship fields associated with the sobject
    */
    public static void validateManualFieldNames(Set<String> manualValueFieldNames, Schema.SObjectType objType, Set<String> relationshipFieldNames)
    {
        // Get set of Strings of object's field names
        List<Schema.SObjectField> allObjFields = objType.getDescribe().fields.getMap().values();
        Set<String> fieldStrings = new Set<String>();
        for(Schema.SObjectField eachField : allObjFields) {
            fieldStrings.add(eachField + '');
        }
        // Add relationship field names as valid fields
        fieldStrings.addAll(relationshipFieldNames);

        // Check that every passed field actually exists on the object
        for(String eachManualField : manualValueFieldNames) {
            if (!fieldStrings.contains(eachManualField) ) {
                throw new DataFactoryException('' + eachManualField);
            }
        }
    }
    
    /*
        Insert a generated nested object and associate it with the child object that
        generated it

        Parameters:
        @    objs - SObjects we are associating with the nested object (example: Account from creating a Contact)
    */
    public static List<SObject> associateNestedObject(List<SObject> objs)
    {
        insert nestedObject;
        for(SObject eachObj : objs){
            eachObj.put(nestedObjName + 'Id', nestedObject.Id);
        }
        return objs;
    }
}
