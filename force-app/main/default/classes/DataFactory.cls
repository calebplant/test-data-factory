public with sharing class DataFactory {

    // Hash for marking a non-required field to retrieve a default value
    public static String POPULATE = 'ec121ff80513ae58ed478d5c5787075b';
    
    /*
        Returns a single sobject with all required fields populated with default values

        Parameters:
        @    objName - String of object name (ex: Account, Contact)
    */
    public static SObject generateObject(String objName)
    {
        Schema.SObjectType newObjType = ((SObject) Type.forName(objName).newInstance()).getSObjectType();
        return generateObject(newObjType, new Map<String, Object>(), 0);
    }

    /*
        Returns a single sobject with all required fields populated with default values
        and any manually passed fields populated. If a parent object is passed to be
        associated with the generated object, it is inserted.

        Parameters:
        @    objName - String of object name (ex: Account, Contact)
        @    manualValues - Map of String=>Object for manually setting values.
             Ex for Contact with parent Account to generate:
                new Map<String, Object> {
                    'FirstName' => 'Frank',
                    'LastName' => 'Smith',
                    'Account' => new Map<String, Object> {
                        'Name' => 'Fort'
                    }
                }
    */
    public static SObject generateObject(String objName, Map<String, Object> manualValues) {
        Schema.SObjectType newObjType = ((SObject) Type.forName(objName).newInstance()).getSObjectType();
        SObject newObj = generateObject(newObjType, manualValues, 0);

        // Check for nested object passed in manualValues. Ex:
        // {'Account' => new Map<String, Object> {'Name' => 'Enron'}}
        if(DataFactoryHelper.nestedObject != null) {
            if(DataFactoryHelper.nestedObject.getSObjectType() + '' != objName) {
                return DataFactoryHelper.associateNestedObject(new List<SObject>{newObj})[0]; // Set lookup field to associated object
            }
        }
        return newObj;
    }

    /*
        Returns a single sobject with all required fields populated with default values
        and any manually passed fields populated

        Parameters:
        @    objName - String of object name (ex: Account, Contact)
        @    manualValues - Map of String=>Object for manually setting values. See above definition for example.
        @    index - Current index of object being generated. Used for generating unique default values in DataFactoryDefaultValues
    */
    public static SObject generateObject(Schema.SObjectType newObjType, Map<String, Object> manualValues, Integer index)
    {
        sObject newObj = newObjType.newSObject();

        ClassFieldInfo fieldInfo = new ClassFieldInfo(newObjType, manualValues);

        // Validate manually passed field Names
        if(manualValues.size() > 0) {
            try {
                DataFactoryHelper.validateManualFieldNames(manualValues.keySet(), newObjType, fieldInfo.relationshipFieldByName.keySet());
            } catch(DataFactoryException e) {
                throw new DataFactoryException('Passed field ' + e.getMessage() + ' was not found on object ' + newObjType.getDescribe().getName(), e);
            }
        }

        // Populate required fields that were not specified manually
        newObj = DataFactoryHelper.populateRequiredFields(newObj, index, fieldInfo.requiredFields);

        // Populate manually specified fields
        newObj = DataFactoryHelper.populateManualFields(newObj, manualValues, fieldInfo);

        return newObj;
    }

    /*
        Returns a list of sobject with all required fields populated with default values

        Parameters:
        @    objName - String of object name (ex: Account, Contact)
        @    numOfObjects - How many records to generate
    */
    public static List<SObject> generateObjectList(String objName, Integer numOfObjects)
    {
        return generateObjectList(objName, numOfObjects, new Map<String, Object>());
    }

    /*
        Returns a list of sobject with all required fields populated with default values
        and any manually passed fields populated

        Parameters:
        @    objName - String of object name (ex: Account, Contact)
        @    numOfObjects - How many records to generate
        @    manualValues - Map of String=>Object for manually setting values. See above definition (generateObject) for example.
    */
    public static List<SObject> generateObjectList(String objName, Integer numOfObjects, Map<String, Object> manualValues)
    {
        List<SObject> newObjList = new List<SObject>();
        Schema.SObjectType newObjType = ((SObject) Type.forName(objName).newInstance()).getSObjectType();

        // Instantiate objects
        for(Integer i=0; i < numOfObjects; i++) {
            newObjList.add(generateObject(newObjType, manualValues, i));
        }

        // If nested object was passed in manualValues, set it
        if(DataFactoryHelper.nestedObject != null) {
            return DataFactoryHelper.associateNestedObject(newObjList);
        }

        return newObjList;
    }
}
