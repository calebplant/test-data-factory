/*
    Helper class for generated default values on required fields and those marked with DataFactory.POPULATE
*/
public with sharing class DataFactoryDefaultValues {

        
    /*
        Returns a single Object with a generated value that will allow the record to be created

        Parameters:
        @    fieldDesc - Field info that will determine what type of Object is returned
        @    index - Passed integer used to generated unique values
    */
    public static Object getValue(Schema.DescribeFieldResult fieldDesc, Integer index) {
        // Check the field type to determine return value.
        // See: https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_enum_Schema_DisplayType.htm
        switch on fieldDesc.getType() {
            when base64 {
                return getBase64(index);
            }
            when Boolean {
                return getBoolean();
            }
            when Currency {
                return getCurrency(index);
            }
            when Date {
                return getDate();
            }
            when Datetime {
                return getDateTime();
            }
            when Email {
                return getEmail(index);
            }
            when Location {
                return getGeolocation(index);
            }
            when Integer, Double {
                return getNumber(index);
            }
            when Percent {
                return getPercent(index);
            }
            when Phone {
                return getPhone();
            }
            when Picklist {
                return getPicklist(fieldDesc);
            }
            when MultiPicklist {
                return getMultiPicklist(fieldDesc);
            }
            when String {
                return getText(index);
            }
            when TextArea {
                return getTextArea(index);
            }
            when Time {
                return getTime();
            }
            when URL {
                return getUrl(index);
            } 
        }
        return null;
    }

    /* Default Values */
    private static Blob getBase64(Integer index) {
        return Blob.valueOf(index.format());
    }

    private static Boolean getBoolean() {
        return false;
    }

    private static Decimal getCurrency(Integer index) {
        return index;
    }

    private static Date getDate() {
        return Date.today();
    }

    private static Datetime getDateTime() {
        return Datetime.now();
    }

    private static String getEmail(Integer index) {
        return 'genericEmail' + index + '@abc.com';
    }

    private static Location getGeolocation(Integer index) {
        return Location.newInstance(0, index);
    }

    private static Decimal getNumber(Integer index) {
        return index;
    }

    private static Decimal getPercent(Integer index) {
        return Math.mod(index, 100);
    }

    private static String getPhone() {
        List<Integer> baseNumber = new List<Integer>{1,1,1,1,1,1,1,1,1,1};
        List<String> finalNum = new List<String>();
        for(Integer eachNumber : baseNumber) {
            finalNum.add('' + Math.mod(Integer.valueOf(eachNumber + Math.random() * 10), 10));
        }
        return String.join(finalNum, '');
    }

    private static String getPicklist(Schema.DescribeFieldResult fieldDesc) {
        if(fieldDesc != null){
            List<Schema.PicklistEntry> pickVals = fieldDesc.getPicklistValues();        
            for (Schema.PicklistEntry pickVal: pickVals) {
                if (pickVal.isDefaultValue()) {
                    return pickVal.getValue(); // default value
                }    
            }
            return pickVals.get(0).getValue(); // first value
        }
        return null;
    }

    private static String getMultiPicklist(Schema.DescribeFieldResult fieldDesc) {
        if(fieldDesc != null){
            List<Schema.PicklistEntry> pickVals = fieldDesc.getPicklistValues();        
            for (Schema.PicklistEntry pickVal: pickVals) {
                if (pickVal.isDefaultValue()) {
                    return pickVal.getValue(); // default value
                }    
            }
            return pickVals.get(0).getValue(); // first value
        }
        return null;
    }

    private static String getText(Integer index) {
        return 'defaultText:' + index.format();
    }

    private static String getTextArea(Integer index) {
        return 'defaultTextArea:' + index.format();
    }

    private static Time getTime() {
        return Time.newInstance(18, 30, 2, 20);
    }

    private static String getUrl(Integer index) {
        return 'http://testurl' + index + '.com';
    }
}
