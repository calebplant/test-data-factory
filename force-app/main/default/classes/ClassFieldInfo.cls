public with sharing class ClassFieldInfo {
    public Map<String, Schema.DescribeFieldResult> relationshipFieldByName = new Map<String, Schema.DescribeFieldResult>();
    public List<Schema.DescribeFieldResult> requiredFields = new List<Schema.DescribeFieldResult>();
    public Map<String, Schema.DescribeFieldResult> optionalFieldByName = new Map<String, Schema.DescribeFieldResult>();

    public ClassFieldInfo(Schema.SObjectType newObjType, Map<String, Object> manualValues)
    {
        // Classify each field
        for (SObjectField eachField : newObjType.getDescribe().fields.getMap().values()) {
            Schema.DescribeFieldResult F = eachField.getDescribe();

            // Is required field and passed in manualValues?
            if (!F.isNillable() && F.isCreateable() && !manualValues.keySet().contains(F.getName())) {
                requiredFields.add(F);
            }
            // Is relationshipfield?
            if(F.getRelationshipName() != null) {
                relationshipFieldByName.put(F.getRelationshipName(), F);
                // relationshipFields.add(F);
                // relationshipFieldNames.add(F.getRelationshipName());
            }
            // Is optional field marked to generate a default value?
            if(manualValues.get(F.getName()) != null) {
                if(manualValues.get(F.getName()) == DataFactory.POPULATE) {
                    optionalFieldByName.put(F.getName(), F);
                }
            }
        }
    }
}
